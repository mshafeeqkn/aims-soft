#!/bin/sh
echo "Reset evaluation started"
rm /Users/shafeequemohammed/Library/Preferences/jetbrains.pycharm.*.plist
rm /Users/shafeequemohammed/Library/Preferences/PyCharm2019.2/eval/PyCharm192.evaluation.key
sed -i'' -e '/evlsprt/d' /Users/shafeequemohammed/Library/Preferences/PyCharm2019.2/options/other.xml
echo "Reset evaluation completed"
