from django.contrib import admin
from django.urls import path, include

from inventory import views

urlpatterns = [
    path('', views.index, name='index'),
    path('admin/', admin.site.urls),
    path('api/', include('inventory.api.urls')),
    path('inventory/', include('inventory.urls')),
]
