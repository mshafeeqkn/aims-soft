from django.contrib import admin

from inventory.models import Customer, Payment, User, Purchase

admin.site.register(User)
admin.site.register(Customer)
admin.site.register(Payment)
admin.site.register(Purchase)
