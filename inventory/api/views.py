from django.http import HttpResponse
from rest_framework import generics
from rest_framework import mixins

from inventory.api.serializers import CustomerSerializer, \
    CustomerDetailSerializer, ExpenseSerializer, CustomerExpenseSerializer
from inventory.models import Customer, Payment


class CustomerView(generics.GenericAPIView,
                   mixins.CreateModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   mixins.ListModelMixin,
                   mixins.RetrieveModelMixin):

    queryset = Customer.objects.all()
    serializer_class = CustomerDetailSerializer

    def get(self, request, pk=None, *args, **kwargs):
        if pk:
            return self.retrieve(request, *args, **kwargs)

        self.serializer_class = CustomerSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, pk=None, *args, **kwargs):
        if pk:
            return self.update(request, *args, **kwargs)
        request.data._mutable = True
        request.data['customer_id'] = CustomerView._get_customer_id('BUILD')
        request.data._mutable = False
        return self.create(request, *args, **kwargs)

    @staticmethod
    def _get_customer_id(construction):
        ret = 'AC'
        ret += construction[0]
        cust_list = Customer.objects.filter(customer_id__startswith=ret)
        last_customer = cust_list.order_by('id')[cust_list.count() - 1]
        next_int = int(last_customer.customer_id[3:]) + 1
        return ret + str(next_int).zfill(3)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ExpenseView(generics.GenericAPIView,
                  mixins.ListModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.CreateModelMixin):
    queryset = Payment.objects.all()
    serializer_class = ExpenseSerializer

    def get(self, request, customer=None, customer_id=None,
            recv=None, *args, **kwargs):
        # Filter data if customer ID is provided
        if customer_id:
            customer = Customer.objects.get(customer_id=customer_id)
            self.queryset = Payment.objects.filter(customer=customer.id)
        else:
            self.queryset = Payment.objects.filter(customer=customer)

        # Filter received/paid expenses
        if recv:
            if recv == 1:
                self.queryset = self.queryset.filter(received=True)
            elif recv == 2:
                self.queryset = self.queryset.filter(received=False)
            elif recv == 0:
                pass
            else:
                self.queryset = Payment.objects.none()
        return self.list(request, *args, **kwargs)

    @staticmethod
    def _get_expense_id():
        expense_num = Payment.objects.count() + 1
        return 'EXP' + str(expense_num).zfill(3)

    def post(self, request, customer, *args, **kwargs):
        request.data._mutable = True
        request.data['expense_id'] = ExpenseView._get_expense_id()
        request.data['customer'] = customer
        request.data._mutable = False
        return self.create(request, *args, **kwargs)


class CustomerExpense(generics.GenericAPIView,
                      mixins.RetrieveModelMixin):

    queryset = Customer.objects.all()
    serializer_class = CustomerExpenseSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class RecentCustomerView(generics.GenericAPIView,
                         mixins.ListModelMixin):
    queryset = Customer.objects.all().order_by('-date_added')[:5]
    serializer_class = CustomerSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, *kwargs)


def index(request):
    return HttpResponse('Rest framework')
