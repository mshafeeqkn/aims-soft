from django.db.models import Sum
from rest_framework import serializers

from inventory.models import Customer, Payment


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('customer_name', 'customer_id', 'phone', 'place')


class CustomerDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class ExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'


class CustomerExpenseSerializer(serializers.ModelSerializer):
    class ExpenseMinSerializer(serializers.ModelSerializer):
        class Meta:
            model = Payment
            fields = ['amount', 'payment_method', 'received']
    expenses = ExpenseMinSerializer(many=True)

    summery = serializers.SerializerMethodField('get_summery')

    # noinspection PyMethodMayBeStatic
    def get_summery(self, customer):
        expenses = Payment.objects.filter(customer=customer.id)
        total_get = expenses.filter(received=True).aggregate(Sum('amount'))
        total_get = total_get['amount__sum']
        total_owe = expenses.filter(received=False).aggregate(Sum('amount'))
        total_owe = total_owe['amount__sum']

        return_data = {
            'amount_get': total_get if total_get else 0,
            'amount_owe': total_owe if total_owe else 0
        }

        balance = return_data['amount_get'] - return_data['amount_owe']
        return_data['received'] = True if balance > 0 else False
        return_data['balance'] = abs(balance)

        return return_data

    class Meta:
        model = Customer
        fields = '__all__'
