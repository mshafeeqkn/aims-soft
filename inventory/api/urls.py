from django.urls import path

from inventory.api.views import CustomerView, index, ExpenseView, \
    CustomerExpense, RecentCustomerView

app_name = 'rest'
urlpatterns = [
    path('customers/', CustomerView.as_view()),
    path('customers/<int:pk>/', CustomerView.as_view()),
    path('customers/<int:pk>/expense', CustomerExpense.as_view()),
    path('customers/<slug:customer_id>/', CustomerView.as_view()),
    path('customers/<int:customer>/expenses/', ExpenseView.as_view()),
    path('customers/<slug:customer_id>/expenses/', ExpenseView.as_view()),
    path('customers/<int:customer>/expenses/<int:recv>/', ExpenseView.as_view()),
    path('recent/customers', RecentCustomerView.as_view()),
    path('', index)
]
