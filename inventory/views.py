from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from datetime import datetime

from inventory.models import Customer, Payment, Purchase, User

MAX_PLACE_DISP_LEN = 19


class GraphData(object):
    MONTH_LIST = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    def __init__(self):
        month = int(timezone.now().strftime('%-m')) - 1
        self.month = self._get_month_list(month)
        self.num_cust = self._get_num_cust_list()
        self.num_comp = self._get_num_comp_list()

    def _get_month_list(self, month):
        month_list = self.MONTH_LIST * 2
        month_list = month_list[month + 12 - 6:month + 12 + 1]
        month_list.reverse()
        return month_list

    def _get_num_cust_list(self):
        num_cust = []
        for month in self.month:
            num_month = self.MONTH_LIST.index(month) + 1
            num_cust.append(Customer.objects.filter(date_added__month=num_month).count())
        return num_cust

    def _get_num_comp_list(self):
        num_cust = []
        for month in self.month:
            num_month = self.MONTH_LIST.index(month) + 1
            num_cust.append(Customer.objects.filter(date_added__month=num_month).count())
        return num_cust


def index(request):
    context = {}
    recent_cust = Customer.objects.all().order_by('-date_added')[:5]
    if recent_cust.count() > 0:
        last_cust_date = recent_cust[0].date_added
        last_cust_date = last_cust_date.strftime("%b %d, %Y")
        context['last_cust_date'] = last_cust_date
        for cust in recent_cust:
            if len(cust.address) > MAX_PLACE_DISP_LEN:
                cust.address = cust.address[:17] + '...'

    recent_payments = Payment.objects.all().order_by().order_by('-payment_date')[:5]
    if recent_payments.count() > 0:
        last_pay_date = recent_payments[0].payment_date
        last_pay_date = last_pay_date.strftime("%b %d, %Y")
        context['last_pay_date'] = last_pay_date

    graph_data = GraphData()

    context.update({
        'graph_data': graph_data,
        'recent_cust': recent_cust,
        'recent_payments': recent_payments,
    })
    return HttpResponse(render(request, 'dashboard.html', context))


def customers(request):
    all_customers = Customer.objects.all()
    context = {
        'all_customers': all_customers
    }
    return HttpResponse(render(request, 'customers.html', context))


def customer_details(request, cust_id):
    customer = get_object_or_404(Customer, pk=cust_id)
    total_purchase = Purchase.objects.filter(customer_id=cust_id, is_draft=False).aggregate(Sum('total_cost'))['total_cost__sum']
    total_payment = Payment.objects.filter(customer_id=cust_id, is_draft=False).aggregate(Sum('amount'))['amount__sum']
    total_purchase = 0 if not total_purchase else total_purchase
    total_payment = 0 if not total_payment else total_payment
    context = {
        'customer': customer,
        'total_purchase': total_purchase,
        'total_payment': total_payment,
        'balance_str': 'Amount due' if total_payment < total_purchase else 'Balance',
        'total_balance': abs(total_purchase - total_payment),
    }
    return HttpResponse(render(request, 'cust_details.html', context))


def new_customer(request):
    context = {}
    if request.method == 'POST':
        customer = Customer()
        customer.customer_id = request.POST['cust-id']
        customer.first_name = request.POST['first-name']
        customer.last_name = request.POST['last-name']
        customer.address = request.POST['address']
        customer.city = request.POST['city']
        customer.country = request.POST['country']
        customer.postal_code = request.POST['postal-code']
        customer.phone = request.POST['phone']
        customer.email = request.POST['email']
        customer.about = request.POST['about']

        if customer.first_name and customer.phone and customer.address:
            customer.save()
            context = {'success_msg': 'New user added successfully'}
        else:
            context = {'error_msg': 'Please fill the mandatory fields marked <span class="text-danger">*</span>'}

    return HttpResponse(render(request, 'new_customer.html', context))


def payment(request, cust_id):
    if request.method == 'POST':
        counter = 0
        Payment.objects.filter(customer_id=cust_id, is_draft=True).delete()
        while True:
            counter_str = str(counter)
            if not request.POST.get('received-by' + counter_str):
                break
            payment_item = Payment()
            payment_item.customer = Customer.objects.get(pk=cust_id)
            aims_user = request.POST['received-by' + counter_str]
            payment_item.received_by = User.objects.get(user_id=aims_user)
            payment_item.payment_method = request.POST['method' + counter_str]
            payment_item.amount = request.POST['payment' + counter_str]
            payment_item.payment_date = request.POST['payment-date' + counter_str]

            post_date = request.POST['payment-date' + counter_str]
            payment_item.payment_date = datetime.strptime(post_date, '%d/%m/%Y').strftime('%Y-%m-%d')
            if request.POST['submit-type'] == 'submit':
                payment_item.is_draft = False
            payment_item.save()
            counter += 1

    if request.POST.get('submit-type') == 'submit':
        return customer_details(request, cust_id)

    draft = Payment.objects.filter(customer_id=cust_id, is_draft=True)
    customer = get_object_or_404(Customer, pk=cust_id)
    users = User.objects.all()
    context = {'customer': customer, 'draft': draft, 'users': users}
    if request.method == 'POST':
        context['success_msg'] = 'Data saved successfully'
    return HttpResponse(render(request, 'payment.html', context))


def purchase(request, cust_id):
    if request.method == 'POST':
        counter = 0
        Purchase.objects.filter(customer_id=cust_id, is_draft=True).delete()
        while True:
            counter_str = str(counter)
            if not request.POST.get('item-name' + counter_str):
                break
            purchase_item = Purchase()
            purchase_item.customer = Customer.objects.get(pk=cust_id)
            purchase_item.item = request.POST['item-name' + counter_str]
            purchase_item.purchase_from = request.POST['purchase-from' + counter_str]
            purchase_item.num_unit = request.POST['num-units' + counter_str]
            purchase_item.unit_price = request.POST['unit-price' + counter_str]
            purchase_item.total_cost = request.POST['total-cost' + counter_str]
            post_date = request.POST['purchase-date' + counter_str]
            purchase_item.purchase_date = datetime.strptime(post_date, '%d/%m/%Y').strftime('%Y-%m-%d')
            if request.POST['submit-type'] == 'submit':
                purchase_item.is_draft = False
            purchase_item.save()
            counter += 1

    if request.POST.get('submit-type') == 'submit':
        return customer_details(request, cust_id)

    draft = Purchase.objects.filter(customer_id=cust_id, is_draft=True)
    customer = get_object_or_404(Customer, pk=cust_id)
    context = {'customer': customer, 'draft': draft}
    if request.method == 'POST':
        context['success_msg'] = 'Data saved successfully'
    return HttpResponse(render(request, 'purchase.html', context))


def edit_payment(request, cust_id, payment_id):
    _payment = Payment.objects.get(pk=payment_id)
    _payment.is_draft = True
    _payment.save()
    draft = Payment.objects.filter(customer_id=cust_id, is_draft=True)
    customer = get_object_or_404(Customer, pk=cust_id)
    users = User.objects.all()
    context = {'customer': customer, 'draft': draft, 'users': users}
    return HttpResponse(render(request, 'payment.html', context))


def edit_purchase(request, cust_id, purchase_id):
    _purchase = Purchase.objects.get(pk=purchase_id)
    _purchase.is_draft = True
    _purchase.save()
    draft = Purchase.objects.filter(customer_id=cust_id, is_draft=True)
    customer = get_object_or_404(Customer, pk=cust_id)
    context = {'customer': customer, 'draft': draft}
    return HttpResponse(render(request, 'purchase.html', context))
