from django.urls import path

from inventory import views

app_name = 'inventory'

urlpatterns = [
    path('customers/', views.customers, name='customers'),
    path('customer/new/', views.new_customer, name='new_customer'),
    path('customer/<int:cust_id>/', views.customer_details, name='cust_detail'),
    path('customer/<int:cust_id>/payment/', views.payment, name='payment'),
    path('customer/<int:cust_id>/purchase/', views.purchase, name='purchase'),
    path('customer/<int:cust_id>/payment/<int:payment_id>/edit', views.edit_payment, name='edit_payment'),
    path('customer/<int:cust_id>/purchase/<int:purchase_id>/edit', views.edit_purchase, name='edit_purchase'),
]
