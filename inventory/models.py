from django.utils import timezone
from django.db import models

PAYMENT_METHODS = [('card', 'Card'), ('online', 'Online'), ('cash', 'Cash')]
PRIVILEGES = [('admin', 'Admin'), ('employee', 'Employee'), ('guest', 'Guest')]


class User(models.Model):
    user_id = models.CharField(max_length=50, blank=True)
    password = models.CharField(max_length=50, blank=True)
    date_added = models.DateTimeField(default=timezone.now, blank=True)
    role = models.CharField(choices=PRIVILEGES, max_length=20, default='employee')

    def __str__(self):
        return str(self.user_id)


class Customer(models.Model):
    customer_id = models.CharField(max_length=25, blank=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    address = models.CharField(max_length=200, blank=True)
    city = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)
    postal_code = models.CharField(max_length=20, blank=True)
    phone = models.CharField(max_length=100, blank=True)
    email = models.CharField(max_length=100, blank=True)
    about = models.CharField(max_length=250, blank=True)
    is_active = models.BooleanField(default=True)
    date_added = models.DateTimeField(default=timezone.now, blank=True)
    balance_amount = models.IntegerField(default=0)
    pending_amount = models.IntegerField(default=0)

    def __str__(self):
        return '%s(%s)' % (self.first_name, self.customer_id)


class Payment(models.Model):
    customer = models.ForeignKey(Customer, related_name='expenses', on_delete=models.PROTECT)
    expense_id = models.CharField(max_length=25)
    amount = models.IntegerField(default=0)
    payment_method = models.CharField(choices=PAYMENT_METHODS, max_length=20)
    received_by = models.ForeignKey(User, related_name='received_by', on_delete=models.PROTECT, default=1)
    payment_date = models.DateTimeField(default=timezone.now)
    is_draft = models.BooleanField(default=True)

    def __str__(self):
        return '%s(%s)' % ('RS:' + str(self.amount), self.customer)


class Purchase(models.Model):
    customer = models.ForeignKey(Customer, related_name='purchase', on_delete=models.PROTECT)
    purchase_id = models.CharField(max_length=25)
    purchase_from = models.CharField(max_length=150)
    item = models.CharField(max_length=200)
    unit_price = models.IntegerField(default=0)
    num_unit = models.IntegerField(default=0)
    total_cost = models.IntegerField(default=0)
    purchase_date = models.DateField(default=timezone.now)
    added_date = models.DateField(default=timezone.now)
    is_draft = models.BooleanField(default=True)

    def __str__(self):
        return '%s(%s)' % (self.item, self.customer)
